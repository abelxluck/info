---
id: 14019
title: Círculo
date: 2019-01-21T14:57:14-04:00
author: n8fr8
layout: page
guid: https://guardianproject.info/?page_id=14019
sourceCode: https://gitlab.com/guardianproject/circulo
menu:
  main:
    parent: apps
---
<div class="wp-block-image">
  <figure class="alignleft is-resized"><img src="https://guardianproject.info/wp-content/uploads/2019/01/mainlogo.png" alt="" class="wp-image-14035" width="141" height="141" srcset="https://guardianproject.info/wp-content/uploads/2019/01/mainlogo.png 1024w, https://guardianproject.info/wp-content/uploads/2019/01/mainlogo-150x150.png 150w, https://guardianproject.info/wp-content/uploads/2019/01/mainlogo-300x300.png 300w, https://guardianproject.info/wp-content/uploads/2019/01/mainlogo-768x768.png 768w" sizes="(max-width: 141px) 100vw, 141px" /></figure>
</div>

#### **Mantén tu comunidad segura con Círculo, una aplicación fácil de usar que proporciona una forma rápida y segura de enviar alertas.**  
<figure class="wp-block-image">

<img src="https://guardianproject.info/wp-content/uploads/2019/01/51271541-707f8800-198d-11e9-94e0-95d903d241cb-1024x676.png" alt="" class="wp-image-14021" srcset="https://guardianproject.info/wp-content/uploads/2019/01/51271541-707f8800-198d-11e9-94e0-95d903d241cb-1024x676.png 1024w, https://guardianproject.info/wp-content/uploads/2019/01/51271541-707f8800-198d-11e9-94e0-95d903d241cb-300x198.png 300w, https://guardianproject.info/wp-content/uploads/2019/01/51271541-707f8800-198d-11e9-94e0-95d903d241cb-768x507.png 768w" sizes="(max-width: 1024px) 100vw, 1024px" /> <figcaption>[Ver la presentación tutorial de la aplicación BETA](https://docs.google.com/presentation/d/1PjBQ2Zub7aFSaef98HETSy5xFzfP21kik_zyi3wB8Hw/edit)</figcaption></figure> 

Círculo es una forma segura de compartir tu ubicación y estado con un grupo de amigos cercanos, familiares o colegas. Te ayuda a crear una red sólida de apoyo entre 6 personas

**Beneficios**

  * Comparte de forma segura tu ubicación
  * Avisa fácilmente a los miembros de tu organización
  * Acceso rápido a tu red de 6 contactos
  * Acceso rápido a números de emergencia en tu área
  * No se mantienen registros



  


**Cómo funciona**

  * Los estados se comparten dentro de la aplicación y se pueden eliminar o actualizar en cualquier momento.
  * Todo lo que se comparte dentro de la aplicación está encriptado y se mantiene seguro en tu teléfono.
  * No se registra ninguna historia. Una vez que elimines tu actualización de estado o la última ubicación, se eliminará para siempre.

**Software Libre**

Este proyecto es software libre y de código abierto. Está disponible en Github en: https://github.com/guardianproject/OpenCircle

**Únete a la prueba beta**

[contact-form-7 id="14023" title="Círculo: Registro de Prueba BETA"]